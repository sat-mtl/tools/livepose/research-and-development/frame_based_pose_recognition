import cv2 as cv
import mediapipe as mp
import argparse
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report, f1_score, recall_score, precision_score
from sklearn.svm import SVC
import pickle
import logging
import sys

logger = logging.getLogger(__name__)

class frame_based_training :

    "Defines a process for training a frame based pose recognition model"

    def init(self, csv_file_name, dev_id, svm_pkl, pose_labels):
        # open the camera feed
        self._camera = cv.VideoCapture(dev_id) #TODO: add argument for changing camera

        if not self._camera.isOpened():
            print("Cannot open camera")
            exit()

        #access MediaPipe solutions
        mp_pose = mp.solutions.pose

        self._pose_recog = mp_pose.Pose(smooth_landmarks=True)

        #create a csv file for the data set
        self._csv_file_name = csv_file_name
        self._data_file = open(csv_file_name, 'a') #TODO: file names should be automatically distributed to avoid confusion with different json inputs

        #create a svm_pkl from the arguments
        self._svm_pkl = svm_pkl

        #create a list of pose labels from the arguments
        self._pose_labels = pose_labels
        
    def image_processed(self, frame):

        # frame processing
        # 1. Convert BGR to RGB
        frame_rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)

        # 2. Flip the img in Y-axis
        frame_flip = cv.flip(frame_rgb, 1)

        # Results
        output = self._pose_recog.process(frame_flip)

        try:
            data = output.pose_landmarks.landmark
            #print(data)
            bad_data = False

            clean = []

            for i,landmark in enumerate(data): 

                clean.append(float(landmark.x))
                clean.append(float(landmark.y))

                # if (landmark.x > 1.0 or landmark.y > 1.0) or (landmark.x < 0. or landmark.y < 0.) : #ignore bad data

                #     bad_data = True

                #     break
                
            if bad_data:
                return
            else:
                return clean
            
        except:
            pass

    def add_data_to_csv(self, frame_data, pose_label):

        good_frames = 0
        bad_frames = 0

        try:
            for id,i in enumerate(frame_data):
                
                self._data_file.write(str(i))
                self._data_file.write(',')

            self._data_file.write(pose_label)
            self._data_file.write('\n')

            good_frames += 1
        
        except:
            bad_frames += 1
            pass
            # self._csv_file_name.write('0')
            # self._csv_file_name.write(',')

            # self._csv_file_name.write('None')
            # self._csv_file_name.write('\n')
        
        print('gesture', pose_label, ': Data was generated for', good_frames, 'frames, and ', bad_frames, 'frames were discarded') 
       

    def generate_svm_model(self):
        
        df = pd.read_csv(self._csv_file_name)

        X = df.iloc[:, :-1]
        Y = df.iloc[:, -1]

        x_train, x_test, y_train, y_test = train_test_split(X.values, Y.values, test_size=0.2, random_state=0)
        svm = SVC(C=10, gamma=0.1, kernel='rbf')
        svm.fit(x_train, y_train)

        with open(self._svm_pkl,'wb') as f:
            pickle.dump(svm,f)

    def training_context(self): 

        """
        main loop that incorporates specific training functions
        """

        num_pose = len(self._pose_labels)
        tick_pose = 0

        i = 0

        training_frames = 60
        training_freq = 5
        training_gap = 25

        print('training pose', tick_pose, ':', self._pose_labels[tick_pose])

        while True: 

            ret, frame = self._camera.read()
            i+=1

            if i > training_gap and i%5 == 0 :

                frame_data = self.image_processed(frame)
                #print(frame_data)
                self.add_data_to_csv(frame_data, self._pose_labels[tick_pose])

            if i > training_frames + training_gap :

                print('finished training pose', tick_pose, ':', self._pose_labels[tick_pose])

                tick_pose += 1

                if tick_pose < num_pose:
                    print('training pose', tick_pose, ':', self._pose_labels[tick_pose])

                i = 0
            
            cv.imshow('frame', frame) #TODO imshow should be processed in the graphical visualisation pipeline

            if cv.waitKey(1) == ord('q') or tick_pose == num_pose: #TODO 500 frame tolerance for data training. Can be changed as desired
                break
            
           
        self._camera.release()
        cv.destroyAllWindows()
        self._data_file.close()

        print('generating svm model')
        self.generate_svm_model()
        print('model generated as ' + self._svm_pkl)




if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--csv-file-name", default="posetraining2.csv", type=str, help="dataset file name")
    parser.add_argument("-id", "--dev-id", default=0, type=int, help="camera device id number")
    parser.add_argument("-pkl", "--svm-pkl", default="posetraining.pkl", type=str, help="pkl filename for svm")
    parser.add_argument("-pose", "--pose-labels", default=[], nargs='+', help="pose labels for training")


    args = parser.parse_args()

    if len(args.pose_labels) < 2:
        logger.error(f"didn't specify enough gestures to train for")
        sys.exit(1)

    fbt = frame_based_training()
    fbt.init(csv_file_name= args.csv_file_name, dev_id = args.dev_id, 
                                            svm_pkl = args.svm_pkl, pose_labels = args.pose_labels)

    fbt.training_context()


    






