## Frame Based Pose Recognition

This directory contains all the scripts necessary to train and execute pose recognition based on data extraction from a live video feed. 

### ![*Recognition example after training on the Y, M, C, A, and wandering poses*](/videos/YMCA.gif)

 The work presented in this directory is based on [dongdv95's hand-gesture-recognition repository](https://github.com/dongdv95/hand-gesture-recognition/tree/main), and makes use of the [mediapipe pose detection framework](https://developers.google.com/mediapipe/solutions/vision/pose_landmarker).

### *pose landmark model from Mediapipe:*
 
<img src="https://developers.google.com/static/mediapipe/images/solutions/pose_landmarks_index.png" width="300">

## Installation and Requirements

The software has been tested on Ubuntu 22.04 and Windows 11, using python 3.10 and a live webcam video feed.  

Requirements to run the scripts in this repository can be found in the requirements.txt file

 ```pip install -r requirements.txt```



## Usage

### ![*Generating a complex interaction system*](/videos/Usage.gif)

We provide scripts for: 
- A) training your own posture recognition models;

- B) devising complex interaction systems making use of multiple posture recognition models applied simultaneously. 

A complete overview of training can be found [in this video](/videos/RealTimeDemo.mov).

### Training your own posture recognition model

This is performed using the `get_pose_training_data.py` script. If we want our model to recognize poses we identify as "Y", "M", "C", "A", and "NotYMCA", we would pass the following command line argument: ` python3 get_pose_training_data.py -pkl ymca_model.pkl -pose Y M C A NotYMCA`. 

This will output a file `ymca_model.pkl`, which can then be used on live video feed for predicting which pose is performed by the user.

### Integrating posture recognition models in a hierarchy-based interaction system

By using the `get_pose_training_data.py` again, we can train a model to recognize zones for which we want to output specific interaction data. For instance, we can run ` python3 get_pose_training_data.py -pkl zone_detection.pkl -pose null_zone hand_up_down pose_training hand_separation`.

Having now two posture recognition models at our disposition, `ymca_model.pkl` and `zone_detection.pkl`, we're ready to test our hierarchy-based interaction system on a live camera feed. This can be done using the `hierarchical_gesture_recog.py` script in the following manner: `python3 hierarchical_gesture_recog.py -c gesture_hierarchy.json`.

The config file `gesture_hierarchy.json` can be modified to generate different interaction systems. We will provide a framework that is more usable to that effect in future updates.


## Introduction to SVM training

A step by step overview for training support vector machines (SVM) can be found in the [Introduction_SVM subdirectory](/Introduction_SVM/). 

## Credits

* Manuel Bolduc: Main contributor
* Bruno Colpron: Video production + User testing
* William Morel : Video production + User testing
* Victor Rios : Presentation of the framework + research




