import cv2
import numpy as np
import pickle
import json
import argparse

import mediapipe as mp
mp_pose = mp.solutions.pose

class hierarchical_gesture_recog :

    "Defines a process for detecting hierarchy based interactions"

    def json_load(self, filename):

        with open(filename) as fp:
            data = json.load(fp)

        # load the zone_svm model
        zone_svm_filename = data.get('model_path')
        with open(zone_svm_filename, 'rb') as f:
            self._zone_svm = pickle.load(f)

        # get the parameters for each type of interactions
        
        interaction_data = data.get('interaction_per_zone')
        # hand_up_down
        hand_up_down = interaction_data.get('hand_up_down')
        self._landmark_tracked = hand_up_down.get('which_hand')
        self._max_height = hand_up_down.get('up')
        self._min_height = hand_up_down.get('down')

        # pose training
        pose_training_data = interaction_data.get('pose_training')
        pose_svm_filename = pose_training_data.get('model_path')
        with open(pose_svm_filename, 'rb') as f:
            self._pose_svm = pickle.load(f)

        # hand_separation
        hand_separation_data = interaction_data.get('hand_separation')
        self._min_sep = hand_separation_data.get('min')
        self._max_sep = hand_separation_data.get('max') 

        return 

    def hand_up_down_detection(self, data):

        y_data = data[int(self._landmark_tracked*2 + 1)]
        y_data = 1.0 - y_data #invert y_axis
        if y_data < self._min_height:
            y_data = self._min_height

        if y_data > self._max_height:
            y_data = self._max_height

        return y_data

    def hand_sep_detection(self, data):

        first_hand = data[30:32]
        second_hand = data[32:34]

        hand_sep = np.linalg.norm(first_hand - second_hand)

        #TODO : integrate min max distance

        return hand_sep
    
    def pose_training_detection(self,data):

        pose_pred = self._pose_svm.predict(data.reshape(-1,66))[0]

        return pose_pred

    def dispatch_detection_by_zone(self, zone_prediction, data, image):

        #cv2 window layouts parameters (TODO: cleanup)
        font = cv2.FONT_HERSHEY_SIMPLEX
    
        # org
        org = (50, 75)
        
        # fontScale
        fontScale = 1
        
        # Blue color in BGR
        color = (255, 255, 255)
        
        # Line thickness of 2 px
        thickness = 2
        
        # Using cv2.putText() method
        image = cv2.putText(image, str(zone_prediction), org, font, 
                        fontScale, color, thickness, cv2.LINE_AA)

        try: 

            if zone_prediction == 'hand_up_down': 
                
                y_data = self.hand_up_down_detection(data)

                # add text to image
                org = (50, 125)
                image = cv2.putText(image, 'y level:' +  str(round(y_data,2)), org, font, 
                            fontScale, color, thickness, cv2.LINE_AA)

            if zone_prediction == 'hand_separation':
                
                hand_sep = self.hand_sep_detection(data)

                # add text to image
                org = (50, 125)
                image = cv2.putText(image, 'distance:' +  str(round(hand_sep,2)), org, font, 
                            fontScale, color, thickness, cv2.LINE_AA)


            if zone_prediction == 'pose_training':
                
                pose_pred = self.pose_training_detection(data)

                # add text to image
                org = (50, 125)
                image = cv2.putText(image, 'pose :' +  str(pose_pred), org, font, 
                            fontScale, color, thickness, cv2.LINE_AA)

            else:  #null zone detection
                pass
        except: 
            pass

        return image


    def detection_context(self, config, dev_id=0):

        #load json parameters for detection
        self.json_load(config)

        cap = cv2.VideoCapture(dev_id)
        with mp_pose.Pose(smooth_landmarks=True) as pose:
            while cap.isOpened():

                success, image = cap.read()
                if not success:
                    print("Ignoring empty camera frame.")
                    # If loading a video, use 'break' instead of 'continue'.
                    continue

                # To improve performance, optionally mark the image as not writeable to
                # pass by reference.
                image.flags.writeable = False
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                image = cv2.flip(image, 1)
                results = pose.process(image)

                try:
                    data = results.pose_landmarks.landmark


                    clean = []
                    #accepted_keypoints = [0, 13, 14, 15, 16, 25, 26, 29, 30]
                    for i,landmark in enumerate(data): 
                    #    if i in accepted_keypoints:
                            clean.append(float(landmark.x))
                            clean.append(float(landmark.y))


                    # for i in without_garbage:
                    #     i = i.strip()
                    #     clean.append(i[2:])

                except:
                    clean = np.zeros([1,66], dtype=int) #could replace this by a null detection      

                image.flags.writeable = True

                image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                image = cv2.flip(image,1)
                image_overlay = image.copy()
                image_overlay = cv2.rectangle(image_overlay, (40,40),(315,140),(0,0,0),-1)
                image = cv2.addWeighted(image_overlay, 0.4, image, 0.6,0,image)
                clean = np.array(clean)
                clean_reshaped = clean.reshape(-1,66)

                zone_pred = self._zone_svm.predict(clean_reshaped)[0]
                #zone_pred = 'hand_separation'

                image = self.dispatch_detection_by_zone(zone_prediction=zone_pred, data=clean, image=image)


               
                
                cv2.imshow('frame', image)
                if cv2.waitKey(1) == ord('q'):
                    break

            cap.release()
            cv2.destroyAllWindows()

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config-file", default="gesture_hierarchy.json", type=str, help="config file")
    parser.add_argument("-id", "--dev-id", default=0, type=int, help="camera device id number")
    
    args = parser.parse_args()

    hgr = hierarchical_gesture_recog()
    hgr.detection_context(config=args.config_file, dev_id=args.dev_id)

